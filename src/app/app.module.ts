import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { DashboardViewComponent } from './modules/dashboard/components/dashboard-view/dashboard-view.component';

@NgModule({
    declarations: [AppComponent],               //  NbThemeModule.forRoot({ name: 'default' }), NbLayoutModule,
    imports: [BrowserModule, AppRoutingModule, BrowserAnimationsModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
