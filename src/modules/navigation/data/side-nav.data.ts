import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSections: SideNavSection[] = [
    /*     {
        text: 'PERSONNEL',
        items: ['dashboard'],
    }, */
    /*     {
        text: 'Personnel',
        items: ['Ventes', 'pages'],
    },
    {
        text: 'Ventes',
        items: ['charts', 'tables'],
    }, */
    {
        text: 'Employés',
        items: ['Tables'],
    },
    {
        text: 'Stock',
        items: ['Todo', 'Todo'],
    },
    {
        text: 'Ventes',
        items: ['Todo', 'Todo'],
    },
    {
        text: 'Clients',
        items: ['Todo', 'Todo'],
    },
];

export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: 'tachometer-alt',
        text: 'Dashboard',
        link: '/dashboard',
    },
    Ventes: {
        icon: 'columns',
        text: 'Ventes',
        submenu: [
            {
                text: 'Static Navigation',
                link: '/dashboard/static',
            },
            {
                text: 'Light Sidenav',
                link: '/dashboard/light',
            },
        ],
    },
    Todo: {
        icon: 'columns',
        text: 'Todo',
        submenu: [
            {
                text: 'Static Navigation',
                link: '/dashboard/static',
            },
            {
                text: 'Light Sidenav',
                link: '/dashboard/light',
            },
        ],
    },

    Employés: {
        icon: 'columns',
        text: 'Employés',
        submenu: [
            {
                text: 'Ajouter un employé',
                link: '/dashboard/static',
            },
            {
                text: 'Editer un employé',
                link: '/dashboard/light',
            },
        ],
    },

    Tables: {
        icon: 'columns',
        text: 'Employés',
        submenu: [
            {
                text: 'Ajouter un employé',
                link: '/dashboard/static',
            },
            {
                text: 'Rechercher un employé',
                link: '/dashboard/light',
            },
            {
                text: 'Editer un employé',
                link: '/dashboard/light',
            },
        ],
    },

    pages: {
        icon: 'book-open',
        text: 'Pages',
        submenu: [
            {
                text: 'Authentication',
                submenu: [
                    {
                        text: 'Login',
                        link: '/auth/login',
                    },
                    {
                        text: 'Register',
                        link: '/auth/register',
                    },
                    {
                        text: 'Forgot Password',
                        link: '/auth/forgot-password',
                    },
                ],
            },
            {
                text: 'Error',
                submenu: [
                    {
                        text: '401 Page',
                        link: '/error/401',
                    },
                    {
                        text: '404 Page',
                        link: '/error/404',
                    },
                    {
                        text: '500 Page',
                        link: '/error/500',
                    },
                ],
            },
        ],
    },
    charts: {
        icon: 'chart-area',
        text: 'Charts',
        link: '/charts',
    },
    tables: {
        icon: 'table',
        text: 'Tables',
        link: '/tables',
    },
};
