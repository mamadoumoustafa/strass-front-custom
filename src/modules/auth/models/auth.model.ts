export interface User {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface UserDetails{
    id:number;
    username: string;
    password: string;
    roles: Role;
    enabled: boolean;
    authorities: Authority;
    accountNonExpired: boolean;
    credentialsNonExpired: boolean;
    accountNonLocked: boolean;
    token: AuthToken;
}

export interface Role{
    name: string;
}
export interface Authority{
    authority: string;
}
export interface AuthToken{
    access_token: string;
    token_type: string;
    refresh_token: string;
    expires_in: number;
    scope: string;
}
export class AuthTokenClass{

    constructor(
       public access_token: string,
       public  token_type: string,
       public refresh_token: string,
       public expires_in: number,
       public  scope: string

    ){}



}