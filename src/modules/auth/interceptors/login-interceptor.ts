import { HttpInterceptor, HttpEvent, HttpHeaders } from '@angular/common/http';
import {HttpRequest,HttpHandler} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginInterceptor implements HttpInterceptor{
    intercept(req :HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {


        const httpOptions = {
            headers: new HttpHeaders({
              'Authorization': 'Basic ' + btoa('username:password')
            })
          };

        
return next.handle(req);
    }


}