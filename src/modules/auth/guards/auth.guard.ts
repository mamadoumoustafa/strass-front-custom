import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

constructor(private router: Router){

}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if(sessionStorage.getItem("scope")== null)
                 return true;
        else{

            this.router.navigate(['/auth/login']);
            return false;

        }
                
        
    }

}
