import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import { AuthService } from '@modules/auth/services';
import { AuthToken, AuthTokenClass } from '@modules/auth/models';
import { Router } from '@angular/router';

@Component({
    selector: 'sb-login',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit {

    authForm: FormGroup;

    constructor(private fb:FormBuilder,
        private authService: AuthService,
        private router: Router) {
        this.authForm = this.fb.group({
            username:  ['',Validators.required],
            password: ['',Validators.required]
        })
    }

    ngOnInit() {

    }
    

    onSubmitForm(){
        console.log(this.authForm);
        this.authService.login(this.authForm.value['username'],this.authForm.value['password'])
        .subscribe(
            (token: AuthTokenClass) =>{
                    console.log(token.scope);
                    sessionStorage.setItem("scope",token.scope);
                    sessionStorage.setItem("oauth2-token",token.access_token);
                    console.log('session storage token:  '+sessionStorage.getItem('oauth2-token'));
                    this.router.navigate(['/dashboard','home'])
            }
        );
    }
}
