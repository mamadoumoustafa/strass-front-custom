import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { UserDetails, AuthToken, AuthTokenClass } from '../models';

@Injectable()
export class AuthService {

     authUrl = 'http://localhost:8079/oauth/token';
     accountUrl='http://localhost:8079/api/account/u/';
    // authUrl =  'http://localhost:8096/api/employe/141';

    

    constructor(private authServer: HttpClient) {}

    getAuth$(): Observable<{}> {
        return of({});
    }

    login(username: string,password: string){

            
            const body = new HttpParams().set('username',username)
            .set('password',password)
            .set('grant_type','password');

            console.log('request body:  '+body.get('password'));
             const httpOptions = {
                headers: new HttpHeaders({
                  'Authorization': 'Basic ' +btoa('Auth-client-admin:Auth-client-secret-admin')
                }),
              }; 


                return  this.authServer.post<AuthTokenClass>(this.authUrl,body,httpOptions);

            
              /* this.authServer.get(this.authUrl).subscribe(

                (data) =>{ console.log(data)}
              ) */

           /*  this.authServer.get(this.authUrl,{      works !
                headers: {
                  Authorization: 'Bearer ' + '861a2545-8d32-4074-9ff1-0355b3749868',
                }
              }).subscribe((data: any) => {
                // Use the data returned by the API
                console.log(data);
          
              }, (err) => {
                console.error(err);
              }); */

        
            
    }

    loadUser(username:string){
      if(username == null)

        return this.authServer.get<UserDetails>(this.accountUrl+username);
    }

    getAuthorizationToken(){
      return sessionStorage.getItem("user");
    }
    
}
