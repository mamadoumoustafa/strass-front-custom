import { EmployeService } from './country.service';
import { TablesService } from './tables.service';

export const services = [TablesService, EmployeService];

export * from './tables.service';
export * from './country.service';
