import { DecimalPipe } from '@angular/common';
import { Injectable, PipeTransform, OnInit } from '@angular/core';
import { COUNTRIES } from '@modules/tables/data/countries';
import { SortDirection } from '@modules/tables/directives';
import { Employe } from '@modules/tables/models';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { EMPS } from '../data/employes';
import { TablesService } from './tables.service';
import { EmployeDTO } from '../models/employesDTO.model';

interface SearchResult {
    countries: EmployeDTO[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: string;
    sortDirection: SortDirection;
}

function compare(v1: number | string, v2: number | string) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(countries: EmployeDTO[], column: string, direction: string): EmployeDTO[] {
    if (direction === '') {
        return countries;
    } else {
        return [...countries].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(Employe: EmployeDTO, term: string, pipe: PipeTransform) {
    return (
        Employe.firstName.toLowerCase().includes(term.toLowerCase()) ||
        Employe.lastName.includes(term) ||
        Employe.fonction.includes(term)
    );
}

@Injectable({ providedIn: 'root' })
export class EmployeService implements OnInit {
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _countries$ = new BehaviorSubject<EmployeDTO[]>([]);
    private _total$ = new BehaviorSubject<number>(0);

    private _state: State = {
        page: 1,
        pageSize: 4,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
    };

    val = 4;

     empArray: EmployeDTO[]=[];


    constructor(private pipe: DecimalPipe,private tableService: TablesService) {


        this.tableService.getEmployes().subscribe(
            (employes:EmployeDTO[]) =>{
                console.log('employes table received'+employes)
                this.empArray = employes;
            }
        )


        this._search$
            .pipe(
                tap(() => this._loading$.next(true)),//Intercepts each emission on the source and runs a function,
                                                    // but returns an output which is identical to the source as long as errors don't occur.
                
                debounceTime(120), // delay values emited by the source Observable
                switchMap(() => this._search()),
                delay(120),
                tap(() => this._loading$.next(false))
            )
            .subscribe(result => {
                this._countries$.next(result.countries);
                this._total$.next(result.total);
            });

        this._search$.next();
    }
    ngOnInit(): void {
        
    }

    get countries$() {
        return this._countries$.asObservable();
    }
    get total$() {
        return this._total$.asObservable();
    }
    get loading$() {
        return this._loading$.asObservable();
    }
    get page() {
        return this._state.page;
    }
    set page(page: number) {
        this._set({ page });
    }
    get pageSize() {
     //   console.log(this._state.pageSize);
        return this._state.pageSize;
    }
    set pageSize(pageSize: number) {
        this._set({ pageSize });
    }
    get searchTerm() {
        return this._state.searchTerm;
    }
    set searchTerm(searchTerm: string) {
        this._set({ searchTerm });
    }
    set sortColumn(sortColumn: string) {
        this._set({ sortColumn });
    }
    set sortDirection(sortDirection: SortDirection) {
        this._set({ sortDirection });
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

        // 1. sort
        let countries = sort(this.empArray, sortColumn, sortDirection);

        // 2. filter
        countries = countries.filter(Employe => matches(Employe, searchTerm, this.pipe));
        // 2-9999
        const total = countries.length;

        // 3. paginate
        countries = countries.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
        return of({ countries, total });
    }
}
