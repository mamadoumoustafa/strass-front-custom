import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EmployeDTO } from '../models/employesDTO.model';

@Injectable()
export class TablesService {

    authUrl = 'http://localhost:8096/api/employe/'; // Gateway server
    

    constructor(private httpClient: HttpClient) {}

    getTables$(): Observable<{}> {
        return of({});
    }

    getEmployes(){

        console.log('token... :'+sessionStorage.getItem("oauth2-token"));

      return  this.httpClient.get<EmployeDTO[]>(this.authUrl,{      
            headers: {
              Authorization: 'Bearer ' + sessionStorage.getItem("oauth2-token"),
            }
          })
    }
}
