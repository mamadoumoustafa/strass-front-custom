import { DecimalPipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';

import { EmployeService } from './country.service';

describe('EmployeService', () => {
    let EmployeService: EmployeService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [EmployeService, DecimalPipe],
        });
        EmployeService = TestBed.get(EmployeService);
    });

    describe('countries$', () => {
        it('should return Observable<Country[]>', () => {
            EmployeService.countries$.subscribe(response => {
                expect(response).toBeDefined();
            });
        });
    });
});
