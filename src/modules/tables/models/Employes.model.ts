export interface Employe {
    [key: string]: string | number;
    id: number;
    nom: string;
    prenom: string;
    fonction: string;
    lien: string;
}
