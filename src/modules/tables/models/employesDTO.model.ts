export interface EmployeDTO {
    [key: string]: string | number;
    id: number;
    firstName: string;
    lastName: string;
    username: string;
    adresse: string;
    telephone: string;
    fonction: string;
    role: string;
}