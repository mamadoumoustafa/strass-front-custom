import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    QueryList,
    ViewChildren,
} from '@angular/core';
import { SBSortableHeaderDirective, SortEvent } from '@modules/tables/directives';
import { Employe } from '@modules/tables/models';
import { EmployeService } from '@modules/tables/services';
import { Observable } from 'rxjs';
import { EmployeDTO } from '@modules/tables/models/employesDTO.model';

@Component({
    selector: 'sb-ng-bootstrap-table',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './ng-bootstrap-table.component.html',
    styleUrls: ['ng-bootstrap-table.component.scss'],
})
export class NgBootstrapTableComponent implements OnInit {
    @Input() pageSize = 4;

    countries$!: Observable<EmployeDTO[]>;
    total$!: Observable<number>;
    sortedColumn!: string;
    sortedDirection!: string;

    @ViewChildren(SBSortableHeaderDirective) headers!: QueryList<SBSortableHeaderDirective>;

    constructor(
        public EmployeService: EmployeService,
        private changeDetectorRef: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.EmployeService.pageSize = this.pageSize;
        this.countries$ = this.EmployeService.countries$;
        this.total$ = this.EmployeService.total$;
    }

    onSort({ column, direction }: SortEvent) {
        this.sortedColumn = column;
        this.sortedDirection = direction;
        this.EmployeService.sortColumn = column;
        this.EmployeService.sortDirection = direction;
        this.changeDetectorRef.detectChanges();
    }


    
}
