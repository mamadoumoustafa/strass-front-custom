import { Employe } from '../models/Employes.model';
import { EmployeDTO } from '../models/employesDTO.model';

export const EMPS: Employe[] = [
    {
        id: 1,
        nom: 'Diop',
        prenom: 'Modou',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    {
        id: 2,
        nom: 'Ndiaye',
        prenom: 'Fallou',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    {
        id: 3,
        nom: 'Diop',
        prenom: 'Issa',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    {
        id: 3,
        nom: 'Sene',
        prenom: 'Fatou',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    {
        id: 3,
        nom: 'Sow',
        prenom: 'Gorgui',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    {
        id: 4,
        nom: 'Diop',
        prenom: 'Maty',
        fonction: 'Vendeur',
        lien: 'voir plus',
    },
    
];

export const EmpArray: EmployeDTO[]=[];

