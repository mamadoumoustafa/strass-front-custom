import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class DashboardGuard implements CanActivate {
    constructor(private router: Router){

    }
    
        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
            if(sessionStorage.getItem("scope")==="all")
                     return true;
            else{
    
                this.router.navigate(['/auth/login']);
                return false;
    
            }
                    
            
        }
}
