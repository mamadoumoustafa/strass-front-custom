import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'sb-new-employe',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './new-employe.component.html',
  styleUrls: ['./new-employe.component.scss']
})
export class NewEmployeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
