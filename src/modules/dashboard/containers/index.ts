import { DashboardComponent } from './dashboard/dashboard.component';
import { LightComponent } from './light/light.component';
import { StaticComponent } from './static/static.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';

export const containers = [DashboardComponent, StaticComponent, LightComponent,DashboardViewComponent];

export * from './dashboard/dashboard.component';
export * from './dashboard-view/dashboard-view.component';
export * from './new-employe/new-employe.component';
export * from './static/static.component';
export * from './light/light.component';
