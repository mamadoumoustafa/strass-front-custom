import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'sb-dashboard-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.scss']
})
export class DashboardViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
