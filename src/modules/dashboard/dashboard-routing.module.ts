/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { DashboardModule } from './dashboard.module';

/* Containers */
import * as dashboardContainers from './containers';

/* Guards */
import * as dashboardGuards from './guards';
import { DashboardGuard } from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'Dashboard - SB Admin Angular',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [DashboardGuard],
        component: dashboardContainers.DashboardComponent, children:[  
  //  component: dashboardContainers.NewEmployeComponent, children:[                                   

            {path:'home', data:{
                title: 'Dashboard - SB Admin Angular',
                breadcrumbs: [
                    {
                        text: 'Home',
                        link:'/dashboard/home',
                        active: true,
                    },
                ],
            },
            component:dashboardContainers.DashboardViewComponent}
            ,

            {path:'new-emp', data:{
                title: 'Dashboard - SB Admin Angular',
                breadcrumbs: [
                    {
                        text: 'Employe',
                        link:'/dashboard/ew-emp',
                        active: true,
                    },
                ],
            },
            component:dashboardContainers.NewEmployeComponent}
        ]
    },
    {
        path: 'static',
        data: {
            title: 'Dashboard Static - SB Admin Angular',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/dashboard',
                },
                {
                    text: 'Static',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.StaticComponent,
    },
    {
        path: 'light',
        data: {
            title: 'Dashboard Light - SB Admin Angular',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/dashboard',
                },
                {
                    text: 'Light',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.LightComponent,
    },
];

@NgModule({
    imports: [DashboardModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
